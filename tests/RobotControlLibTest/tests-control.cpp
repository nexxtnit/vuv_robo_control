#include "RobotControl.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "Movement.hpp"
#include "RobotMonitor.hpp"


TEST(ManualMovement, LeftRightAxis_Right) {
  // Arrange
    ui_in.button1 = false;
  ui_in.button2 = true;
  robot_in.limit_right = false;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_TRUE(robot_out.motor_right);
  ASSERT_TRUE(robot_out.motor_dir_right);
}

TEST(ManualMovement, LeftRightAxis_Left) {
  // Arrange
  ui_in.button1 = true;
  ui_in.button2 = false;
  robot_in.limit_right = false;
  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_TRUE(robot_out.motor_right);
  ASSERT_FALSE(robot_out.motor_dir_right);
}

TEST(ManualMovement, LeftRightAxis_Off) {
  // Arrange
  ui_in.button2 = false;
  ui_in.button1 = false;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_FALSE(robot_out.motor_right);
}

TEST(ManualMovement, UpDownAxis_Up) {
  // Arrange
  ui_in.button3 = true;
  ui_in.button4 = false;
  robot_in.limit_up = false;
  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_TRUE(robot_out.motor_up);
  ASSERT_TRUE(robot_out.motor_dir_up);
}

TEST(ManualMovement, UpDownAxis_Down) {
  // Arrange
  ui_in.button3 = false;
  ui_in.button4 = true;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_TRUE(robot_out.motor_up);
  ASSERT_FALSE(robot_out.motor_dir_up);
}

TEST(ManualMovement, UpDownAxis_Off) {
  // Arrange
  ui_in.button3 = false;
  ui_in.button4 = false;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_FALSE(robot_out.motor_up);
}

TEST(ManualMovement, PushPullAxis_Push) {
  // Arrange
  ui_in.button5 = true;
  ui_in.button6 = false;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_TRUE(robot_out.motor_back);
  ASSERT_FALSE(robot_out.motor_dir_back);
}

TEST(ManualMovement, PushPullAxis_Pull) {
  // Arrange
  ui_in.button5 = false;
  ui_in.button6 = true;
  robot_in.limit_back = false;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_TRUE(robot_out.motor_back);
  ASSERT_TRUE(robot_out.motor_dir_back);
}

TEST(ManualMovement, PushPullAxis_Off) {
  // Arrange
  ui_in.button5 = false;
  ui_in.button6 = false;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_FALSE(robot_out.motor_back);
}

TEST(ManualMovement, Gripper_Open) {
  // Arrange
  ui_in.button7 = false;
  ui_in.button8 = true;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_TRUE(robot_out.motor_open);
  ASSERT_TRUE(robot_out.motor_dir_open);
}

TEST(ManualMovement, Gripper_Close) {
  // Arrange
  ui_in.button7 = true;
  ui_in.button8 = false;
  robot_in.limit_back = false;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_TRUE(robot_out.motor_open);
  ASSERT_FALSE(robot_out.motor_dir_open);
}

TEST(ManualMovement, Gripper_Off) {
  // Arrange
  ui_in.button7 = false;
  ui_in.button8 = false;

  RobotMovement& robotMovement = RobotMovement::instance();
  MonitorRobot& monitor = MonitorRobot::instance();
  monitor.setIsReady(true);

  // Act
  robotMovement.manualMode();

  // Assert
  ASSERT_FALSE(robot_out.motor_open);
}
