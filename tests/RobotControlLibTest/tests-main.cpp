#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "RobotControl.h"

ui_in_struct ui_in;
ui_out_struct ui_out;
robot_in_struct robot_in;
robot_out_struct robot_out;

int main(int argc, char** argv)
{
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
