#include "Movement.hpp"
#include "RobotControl.h"
#include "RobotMonitor.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"


TEST(CheckButtons, bothXButtonsPressed) {
  MonitorRobot& monitor = MonitorRobot::instance();
  ui_in.button1 = true;
  ui_in.button2 = true;
  ASSERT_FALSE(monitor.checkAxisButtons());
}
TEST(CheckButtons, bothYButtonsPressed) {
  MonitorRobot& monitor = MonitorRobot::instance();
  ui_in.button3 = true;
  ui_in.button4 = true;
  ASSERT_FALSE(monitor.checkAxisButtons());
}
TEST(CheckButtons, bothZButtonsPressed) {
  MonitorRobot& monitor = MonitorRobot::instance();
  ui_in.button5 = true;
  ui_in.button6 = true;
  ASSERT_FALSE(monitor.checkAxisButtons());
}
TEST(CheckButtons, bothGripperButtonsPressed) {
  MonitorRobot& monitor = MonitorRobot::instance();
  ui_in.button7 = true;
  ui_in.button8 = true;
  ASSERT_FALSE(monitor.checkAxisButtons());
}
TEST(CheckButtons, bothModeButtonsPressed) {
  MonitorRobot& monitor = MonitorRobot::instance();
  ui_in.buttonfree = true;
  ui_in.buttonmode = true;
  ASSERT_FALSE(monitor.checkModeButtons());
}

TEST(CheckAxisLimits, LimitSwitchX) {
  MonitorRobot& monitor = MonitorRobot::instance();
  RobotMovement& robotMovement = RobotMovement::instance();
  robot_in.limit_right = true;
  robotMovement.xaxis_clockwise=true;
  ASSERT_FALSE(monitor.checkLimitSwitches());
}
TEST(CheckAxisLimits, LimitSwitchY) {
  MonitorRobot& monitor = MonitorRobot::instance();
  RobotMovement& robotMovement = RobotMovement::instance();
  robot_in.limit_back = true;
  robotMovement.yaxis_pull=true;
  ASSERT_FALSE(monitor.checkLimitSwitches());
}
TEST(CheckAxisLimits, LimitSwitchZ) {
  MonitorRobot& monitor = MonitorRobot::instance();
  RobotMovement& robotMovement = RobotMovement::instance();
  robot_in.limit_up = true;
  robotMovement.zaxis_up=true;
  ASSERT_FALSE(monitor.checkLimitSwitches());
}
TEST(CheckAxisLimits, LimitSwitchGripper) {
  MonitorRobot& monitor = MonitorRobot::instance();
  RobotMovement& robotMovement = RobotMovement::instance();
  robot_in.limit_open = true;
  robotMovement.gripper_open=true;
  ASSERT_FALSE(monitor.checkLimitSwitches());
}