# 1. Inhaltsverzeichnis

<!-- TOC -->

- [1. Inhaltsverzeichnis](#1-inhaltsverzeichnis)
- [2. Git Repository](#2-git-repository)
- [3. Projektstruktur (CMake)](#3-projektstruktur-cmake)
- [4. Verwendete Bibliotheken (Conan Paket-Manager)](#4-verwendete-bibliotheken-conan-paket-manager)
- [5. IDE Unterstützung](#5-ide-unterst%c3%bctzung)
  - [5.1. Visual Studio 2017 / 2019: (Windows)](#51-visual-studio-2017--2019-windows)
  - [5.2. CLion (Windows, Linux, MacOS)](#52-clion-windows-linux-macos)
  - [5.3. Visual Studio Code (Windows, Linux, MacOS)](#53-visual-studio-code-windows-linux-macos)
- [6. Initiale Konfiguration und Kompilation](#6-initiale-konfiguration-und-kompilation)
  - [6.1. Allgemeiner Ablauf](#61-allgemeiner-ablauf)
  - [6.2. Windows 10 - Visual Studio 2017 / Visual Studio 2019](#62-windows-10---visual-studio-2017--visual-studio-2019)
    - [6.2.1. Ablauf über Kommandozeile (CMD / Powershell)](#621-ablauf-%c3%bcber-kommandozeile-cmd--powershell)
    - [6.2.2. Workaround Visual Studio 2019 GLEW Release Build (memset Linker-Fehler)](#622-workaround-visual-studio-2019-glew-release-build-memset-linker-fehler)
    - [6.2.3. Hinzufügen von neuen Quell- und Headerdateien zu einem CMake Projekt unter Visual Studio 2017 / Visual Studio 2019](#623-hinzuf%c3%bcgen-von-neuen-quell--und-headerdateien-zu-einem-cmake-projekt-unter-visual-studio-2017--visual-studio-2019)
  - [6.3. Ubuntu 19.10 - CLion](#63-ubuntu-1910---clion)
    - [6.3.1. Ablauf über Kommandozeile (Bash, ZSH, Fish)](#631-ablauf-%c3%bcber-kommandozeile-bash-zsh-fish)
- [7. Troubleshooting](#7-troubleshooting)
  - [7.1. Allgemein](#71-allgemein)
  - [7.2. Windows](#72-windows)
  - [7.3. Linux / MacOS](#73-linux--macos)

<!-- /TOC -->

# 2. Git Repository

Der Quellcode des Projekt ist auf der [HTW Redmine Instanz gehostet](https://pm.f1.htw-berlin.de/redmine/projects/ce_m14_verifikation_und_validierung). Das dazugehörige Git Repository lautet [https://pm.f1.htw-berlin.de/git/ce_m14_verifikation_und_validierung.git](https://pm.f1.htw-berlin.de/git/ce_m14_verifikation_und_validierung.git).

# 3. Projektstruktur (CMake)

Das Projekt RobotControl besteht aus einem Hauptprojekt welches mehrere Bibliotheken, derzeit eine Anwendung und passenden Unit-tests enthält. Jedes dieser Unterprojekte liegt in einem eigenem Verzeichnis als CMake Projekt und definiert in der jeweiligen CMakeLists.txt seine Abhängigkeiten und Eigenschaften. 

[CMake](https://cmake.org/) ist ein Open Source Buildsystem welches auf allen gängigen Betriebssystemen funktioniert. Die Konfigurationsdateien - CMakeLists.txt sind Platform und Compiler unabhängig, aus diesen können platform spezifische Arbeitsmappen und Buildfiles wie Makefiles, Ninja oder Visual Studio Solutions generiert werden. Außerdem gibt es eine direkte Unterstützung für CMake Projekte für viele wichtige Entwicklungsumgebungen, womit die Generierung und Verwaltung im Hintergrund abläuft. Letzters Vorgehen wird sehr empfohlen.

Tutorial / Buch: [Modern CMake](https://cliutils.gitlab.io/modern-cmake/)

```
RobotControl (Hauptprojekt)
├── CMakeLists.txt (CMake Datei für das Hauptprojekt)
├── conanfile.txt (Benutzte Bibliotheken für die Conan Paketverwaltung) 
├── Readme.md
├── apps
│  └── SoftwareUi (Benutzer*innen Oberfläche - diese wird nicht verändert!)
│     ├── CMakeLists.txt
│     ├── main.cpp
│     ├── UserIO.cpp
│     └── UserIO.h
├── external (Externe Abhängigkeiten wie Bibliotheken)
│  └── imgui-bindings (Dear ImGui Bindings für OpenGl 3 und GLFW)
│     ├── CMakeLists.txt
│     ├── imgui_impl_glfw.cpp
│     ├── imgui_impl_glfw.h
│     ├── imgui_impl_opengl3.cpp
│     └── imgui_impl_opengl3.h
├── src (Bibliotheken)
│  ├── RobotControlLib (Robotersteuerung - die Funktionalität ist hier zu implementieren)
│  │  ├── CMakeLists.txt
│  │  └── RobotControl.cpp
│  ├── RobotLib (Interface Bibliothek - diese wird nicht verändert!)
│  │  ├── CMakeLists.txt
│  │  └── RobotControl.h
│  └── RobotSimulateLib (Robotersimulation - diese wird nicht verändert!)
│     ├── CMakeLists.txt
│     ├── RobotSimulate.cpp
│     └── RobotSimulate.h
└── tests (Unit-tests)
   └── RobotControlLibTest (Unit-tests für die Robotersteuerung)
      ├── CMakeLists.txt
      ├── tests-control.cpp 
      └── tests-main.cpp
```

# 4. Verwendete Bibliotheken (Conan Paket-Manager)

RobotControl nutzt externe Bibliotheken welche durch den [C/C++ Paket-Manager Conan](https://conan.io/) für das Projekt heruntergeladen werden und als CMake Targets Konfiguriert damit diese im Hauptprojekt RobotControll und allen Unterprojekten genutzt werden können. Dadurch entfällt bist auf die Ersteinrichtung eine Aufwendige Konfiguration und manuelle Verwaltung. In der Datei `conanfile.txt` sind alle Bibliotheken aufgelistet.

* [Dear ImGui](https://github.com/ocornut/imgui) "Bloat-free Immediate Mode Graphical User interface for C++ with minimal dependencies"
  * GUI-Library mit der die Benutzer*innen Oberfläche im Projekt SoftwareUi gebaut ist
* [Googletest](https://github.com/google/googletest) - Google Testing and Mocking Framework
  * Unit-testing Library mit der alle Unit-tests umgesetzt werden. Eine Dokumentation befindet sich auf [Github](https://github.com/google/googletest/tree/master/googletest/docs).
* [Googletest Mocking (gMock) Framework](https://github.com/google/googletest)
  * Mocking Library mit der Unit-test weiter isoliert werden können. Eine Dokumentation befindet sich auf [Github](https://github.com/google/googletest/blob/master/googlemock/README.md).
* [GLFW](https://www.glfw.org/) Open Source, multi-platform library for OpenGL, OpenGL ES and Vulkan development on the desktop
  * Wird von Dear ImGui benötigt, für RobotControl nur indirekt interessant
* [GLEW](http://glew.sourceforge.net/) The OpenGL Extension Wrangler Library
  * Wird von Dear ImGui benötigt, für RobotControl nur indirekt interessant

Tutorial / Dokumentation:  [docs.conan.io](https://docs.conan.io/en/latest/introduction.html)

# 5. IDE Unterstützung

## 5.1. Visual Studio 2017 / 2019: (Windows)

Seit Visual Studio 2017 werden CMake Projekte nativ unterstützt. Eine [Umfangreiche](https://docs.microsoft.com/en-us/cpp/build/cmake-projects-in-visual-studio?view=vs-2019) Dokumentation findet sich in der offiziellen Dokumentation von Microsoft. Ebenfalls werden Unit-tests mit Google Test durch [Visual Studio unterstützt](https://docs.microsoft.com/en-us/visualstudio/test/how-to-use-google-test-for-cpp?view=vs-2019).

**Achtung:** CMake Projekte können auch in Visual Studio Solutions konvertiert werden - diese Anleitung geht aber davon aus das die native Unterstützung von CMake in Visual Studio genutzt wird!

## 5.2. CLion (Windows, Linux, MacOS)

Clion unterstützt von Haus aus CMake Projekte und Unit-tests mit Google Test.

## 5.3. Visual Studio Code (Windows, Linux, MacOS)

Für VSCode existieren mehrere Extentions mit der die Entwicklung von C/C++, CMake und Google Test Projekten möglich wird.

* C/C++ - ms-vscode.cpptools
* Catch2 and Google Test Explorer - matepek.vscode-catch2-test-adapter
* CMake - twxs.cmake
* CMake Tools - ms-vscode.cmake-tools
* Test Explorer UI - hbenl.vscode-test-explorer

# 6. Initiale Konfiguration und Kompilation

Um RobotControl und alle Unterprojekte erfolgreich zu erstellen müssen alle externen Abhängigkeiten - sprich Bibliotheken mit Conan heruntergeladen und für jeden CMake Build z.B. Debug und Release konfiguriert werden. Dieser Schritt muss also für jeden Build Typ jeweils einmalig für das komplette Projekt durchgeführt werden! Danach kann das komplette Projekt einfach über die Entwicklungsumgebung gebaut werden. Wenn diese Schritte nicht ausgeführt werden, oder zum Beispiel Abhängigkeiten wie Headerdateien von Google Test oder Dear Imgui nicht gefunden werden ist hier etwas schiefgelaufen.

Je nach Betriebssystem und Entwicklungsumgebung sind diese Schritte etwas unterschiedlich, aber übertragbar. Sie unterscheiden sich nur nach den Build Ordnern der Entwicklungsumgebungen - hier liegt auch die häufigste Fehlerquelle. Zum Beispiel CLion nennt seine Build Ordner `cmake-build-debug` bzw. `cmake-build-release` und Visual Studio `out/build/x64-Debug` bzw. `out/build/x64-Relase`.

## 6.1. Allgemeiner Ablauf

1. Alle Vorraussetzungen installieren - Conan, CMake, Python3
2. Quellcode von RobotControll auschecken
3. Conan Default Profil nach Erstinstallation einrichten
4. (Linux/MacOS) Conan C++11 ABI Standard einstellen
5. Conan bincrafters Repository hinzufügen
6. Conan Abhängigkeiten herunterladen und im Build Ordner konfigurieren
7. Das RobotControl Hauptprojekt in der IDE öffnen
8. Etwas warten bis das CMake Projekt komplett geladen und initial konfiguriert wurde. Falls es hier zu Fehlern kommt ist etwas bei Schritt 6 schiefgelaufen. (Wird ein anderer Build Ordner von der IDE erwartet als eingerichtet wurde?)
9.  Fertig! :-)


## 6.2. Windows 10 - Visual Studio 2017 / Visual Studio 2019

Vorraussetzungen:

* [Visual Studio 2017 Community](https://visualstudio.microsoft.com/de/thank-you-downloading-visual-studio/?sku=Community&rel=15) oder
* [Visual Studio 2019 Community](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16)
    * Bei der Installation muss als Workload "Desktopentwicklung mit C++" ausgewählt werden. Bitte überprüfen Sie auch ob die Einzelkomponenten "Testadapter für Google Test" und "C++-CMake-Tools für Windows" installiert werden. 
* [Git](https://git-scm.com/download/win) (Bitte den Windows Installer 64 bit nutzen)
* [CMake](https://cmake.org/download/#latest) (Bitte den Windows Installer x64 nutzen)
* [Conan](https://conan.io/downloads.html) (Bitte den Windows Installer x86_64 nutzen)
* OpenGL 3.0 fähige Grafikkarte
  * Intel
  * Nvidia
  * AMD
  * [Mesa3D](https://fdossena.com/?p=mesa/index.frag) - Software Renderer für Virtuelle Maschinen oder PCs die keine Unterstützung für OpenGL 3.0 haben

**Wichtig:** Nach der Installion bitte überprüfen ob Git, Conan und CMake über eine Kommandozeile wie z.B. (CMD, Powershell) verfügbar sind. Falls nicht befinden sich die Programme nicht in der Umgebungsvariable (Path), daher werden sie nicht von Windows gefunden. Falls ein Neustart nichts bringt müssen diese manuell hinzugefügt werden.

### 6.2.1. Ablauf über Kommandozeile (CMD / Powershell)

Sind alle Vorraussetzungen bereits installiert? Falls nicht diese bitte vorher installieren!

1. `git clone https://pm.f1.htw-berlin.de/git/ce_m14_verifikation_und_validierung.git` Quellcode über git klonen.
2. CMD oder Powershell im RobotControl Verzeichnis öffnen
3. Conan konfigurieren  
   1. `conan profile new default --detect`  Conan - Default Profil nach Erstinstallation einrichten
   2. `conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan` Conan - bincrafters Repository hinzufügen
4. Debug Build einrichten
   1. `mkdir .\out\build\x64-Debug` Buildverzeichnis anlegen falls nicht vorhanden - Achtung es wird von dem Standard VS Build Pfad ausgegangen, falls ein anderer für VS erwartet wird müssen diese Schritte entsprechend für den korrekten Pfad wiederholt werden!
   2. `conan install conanfile.txt -s build_type=Debug --build=missing -if .\out\build\x64-Debug` Conan Abhängigkeiten herunterladen
5. Release Build einrichten
   1. `mkdir .\out\build\x64-Release` Buildverzeichnis anlegen falls nicht vorhanden - Achtung es wird von dem Standard VS Build Pfad ausgegangen, falls ein anderer für VS erwartet wird müssen diese Schritte entsprechend für den korrekten Pfad wiederholt werden!
   2. `conan install conanfile.txt -s build_type=RelWithDebInfo --build=missing -if .\out\build\x64-Release` Conan Abhängigkeiten herunterladen
   3. **Achtung:** Tritt mit VS2019 folgender Fehler bei der Kompilierung von GLEW auf muss der Workaround der unter "Workaround VS2019 Release Build" beschrieben ist angewendet werden.
      > glew.obj : error LNK2019: Verweis auf nicht aufgelöstes externes Symbol "memset" in Funktion "glewContextInit". [C:\Users\karlk\.conan\data\glew\2.1.0\bincrafters\stable\build\75ed07303f056424be45ffb582032fe7ad980a95\_source_subfolder\build\cmake\glew.vcxproj]       
6. Alle externen Abhängigkeiten in external Ordner kopieren
   1. `xcopy .\out\build\external .\external /e /y`
7. RobotControl in Visual Studio (VS) einrichten   
   1. VS starten und den Startbildschrim mit "Ohne Code fortfahren" überspringen
   2. In der Hauptansicht über den Reiter "Datei - Öffnen - CMake" die `CMakeLists.txt` im RobotControl Ordner öffnen.
   3. VS konfiguriert nun den Debug-Build, wenn keine Fehler aufgetreten sind kann das komplette Projekt über "Erstellen - Alle erstellen" kompiliert werden. 
8. RobotControl Release-Build in Visual Studio einrichten
   1. Die CMake-Einstellungen über den Reiter "Projekt - CMake-Einstellungen für "RobotControl" öffnen. 
   2. Eine neue Konfiguration über die Plus Schaltfläche anlegen und "x64-Release" auswählen
   3. VS konfiguriert nun den Release-Build, wenn keine Fehler aufgetreten sind kann das komplette Projekt über "Erstellen - Alle erstellen" kompiliert werden.
9. Fertig :-). Die Unit-tests können nach dem Erstellen im Test-Explorer gefunden werden.

### 6.2.2. Workaround Visual Studio 2019 GLEW Release Build (memset Linker-Fehler)

Unter VS2019 gibt es ein Kompilierungsfehler mit der Release Version der GLEW Bibliothek unter der aktuellen Version 2.1.0 - die Funktion memset ist nicht bekannt. Version 2.2.0 behebt dies, allerding ist diese noch nicht veröffentlicht. Glücklicherweise ist dieser [Fehler bekannt](https://github.com/nigels-com/glew/issues/238) und es existiert ein Workaround. Dazu müssen folgende Zeilen in die CMakeLists.txt der GLEW Bibliothek nach den Zeilen

```
# remove stdlib dependency
target_link_libraries (glew LINK_PRIVATE -nodefaultlib -noentry)
```
eingefügt werden:

```
target_link_libraries (glew LINK_PRIVATE libvcruntime.lib)
target_link_libraries (glew LINK_PRIVATE msvcrt.lib )
```

Endergebnis:

```
# remove stdlib dependency
target_link_libraries (glew LINK_PRIVATE -nodefaultlib -noentry)
target_link_libraries (glew LINK_PRIVATE libvcruntime.lib)
target_link_libraries (glew LINK_PRIVATE msvcrt.lib )
```

Die CMakeLists.txt findet sich im `.conan` Verzeichnis im Home Verzeichnis des aktuellen Nutzers! (Falls der Pfad nicht funktionieren sollte, bitte die entsprechende Zeile per Volltextsuche im .conan Verzeichnis suchen)

`%HOMEPATH%\.conan\data\glew\2.1.0\bincrafters\stable\source\_source_subfolder\build\cmake\CMakeLists.txt`

Zum Vergleich der dazugehörige Commit der GLEW 2.2.0 Bibliothek https://github.com/nigels-com/glew/commit/4bbe8aa2ab70a6eb847ee5751735422d0ba623cd

Danach kann conan erneut aufgerufen werden, die Kompilierung funktioniert nun.

### 6.2.3. Hinzufügen von neuen Quell- und Headerdateien zu einem CMake Projekt unter Visual Studio 2017 / Visual Studio 2019

Neue Dateien für ein CMake Projekt müssen der `SOURCES` bzw. `HEADERS` Variable in der `CMakeLists.txt` des Projekt hinzugefügt werden. Als Beispiel soll der RobotControlLib eine `Robot` Klasse mit den Dateien `Robot.cpp`  und `Robot.h` hinzugefügt werden. Welche dann in der `RobotControl.cpp` genutzt wird. Hier der Ausgangszustand der `CMakeLists.txt`.

```
PROJECT(RobotControlLib)

set(SOURCES
        RobotControl.cpp
        )

set(HEADERS
        )

add_library(${PROJECT_NAME} ${SOURCES} ${HEADERS})

# Make sure the compiler can find include files for our library
# when other libraries or executables link to it
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(${PROJECT_NAME} RobotLib)
```

Und die Ordnerstruktur:

```
RobotControlLib
├── CMakeLists.txt
└── RobotControl.cpp
```

Mit Hilfe des Visual Studio Projektmappen Explorer können die Dateien `Robot.cpp`  und `Robot.h` im RobotControlLib Ordner angelegt werden. Danach wird `Robot.cpp` der `SOURCES` Variable und `Robot.h` der `HEADERS` Variable händisch in der `CMakeLists.txt` hinzugefügt:

```
PROJECT(RobotControlLib)

set(SOURCES
        RobotControl.cpp
        Robot.cpp
        )

set(HEADERS
        Robot.h
        )

add_library(${PROJECT_NAME} ${SOURCES} ${HEADERS})

# Make sure the compiler can find include files for our library
# when other libraries or executables link to it
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(${PROJECT_NAME} RobotLib)
```

In der `RobotControl.cpp` kann diese Klasse nun einfach inkludiert werden.

```
#include "RobotControl.h"
#include "Robot.h"

void control()
{
}
```

## 6.3. Ubuntu 19.10 - CLion

Vorraussetzungen:

* [CLion](https://www.jetbrains.com/clion/) Kostenlos für Studierende über das [Jetbrains Student Program](https://www.jetbrains.com/student/)
* Git
* CMake
* Conan
* Python 3
* GCC
* OpenGL 3.0 fähige Grafikkarte
  * Intel
  * Nvidia
  * AMD  

Bis auf CLion können alle Vorraussetzungen über die Paketverwaltung installiert werden.

### 6.3.1. Ablauf über Kommandozeile (Bash, ZSH, Fish)

1. `git clone https://pm.f1.htw-berlin.de/git/ce_m14_verifikation_und_validierung.git` Quellcode über git klonen.
2. Terminal im RobotControl Verzeichnis öffnen
3. Vorraussetzungen installieren
   1. `sudo apt install -y build-essential cmake python3 python3-pip git` GCC, Python3, CMake und Git installieren
   2. `sudo pip3 install conan` Conan über die Python3 Paketverwaltung installieren    
4. Conan konfigurieren  
   1. `conan profile new default --detect`  Conan - Default Profil nach Erstinstallation einrichten
   2. `conan profile update settings.compiler.libcxx=libstdc++11 default` Conan C++11 ABI Standard einstellen
   3. `conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan` Conan - bincrafters Repository hinzufügen   
5. Debug Build einrichten
   1. `mkdir cmake-build-debug` Buildverzeichnis anlegen falls nicht vorhanden - Achtung es wird von dem Standard CLion Build Pfad ausgegangen, falls ein anderer für Clion erwartet wird müssen diese Schritte entsprechend für den korrekten Pfad wiederholt werden!
   2. `conan install conanfile.txt -s build_type=Debug --build=missing -if cmake-build-debug` Conan Abhängigkeiten herunterladen
6. Release Build einrichten
   1. `mkdir cmake-build-release` Buildverzeichnis anlegen falls nicht vorhanden - Achtung es wird von dem Standard CLion Build Pfad ausgegangen, falls ein anderer für Clion erwartet wird müssen diese Schritte entsprechend für den korrekten Pfad wiederholt werden!
   2. `conan install conanfile.txt -s build_type=Release --build=missing -if cmake-build-release` Conan Abhängigkeiten herunterladen
7. RobotControl in CLion einrichten   
   1. Clion starten und über "Open" die `CMakeLists.txt` im RobotControl Ordner öffnen.
   2. Clion konfiguriert nun die Build varianten
8. Die Unit-tests lassen sich über das Target "RobotControlLibTest" ausführen. Clion bietet auch eine gute [Online Hilfe](https://www.jetbrains.com/help/clion/performing-tests.html).
9.  Fertig :-)

# 7. Troubleshooting

## 7.1. Allgemein

* Conan Verzeichnis (.conan) im Homeverzeichnis löschen und komplett neu beginnen 

## 7.2. Windows

* TBC

## 7.3. Linux / MacOS

* GCC als C/C++ Compiler nutzen und Mischung von GCC und Clang vermeiden.