#include "RobotControl.h"
#include "imgui.h"

using namespace std;

const std::uint16_t Key_W = 0x57;
const std::uint16_t Key_S = 0x53;
const std::uint16_t Key_A = 0x41;
const std::uint16_t Key_D = 0x44;
const std::uint16_t Key_1 = 0x31;
const std::uint16_t Key_2 = 0x32;

void process_input(const bool keys[512])
{
    ui_in.button1 = keys[ImGui::GetKeyIndex(ImGuiKey_LeftArrow)];
    ui_in.button2 = keys[ImGui::GetKeyIndex(ImGuiKey_RightArrow)];
    ui_in.button3 = keys[ImGui::GetKeyIndex(ImGuiKey_UpArrow)];
    ui_in.button4 = keys[ImGui::GetKeyIndex(ImGuiKey_DownArrow)];
    ui_in.button5 = keys[Key_W];
    ui_in.button6 = keys[Key_S];
    ui_in.button7 = keys[Key_A];
    ui_in.button8 = keys[Key_D];
    ui_in.buttonmode = keys[Key_1];
    ui_in.buttonfree = keys[Key_2];
}
