#pragma once

#include <string>

//#include "RobotMonitor.hpp"
// user interface

struct ui_in_struct {
  // direction pushbuttons on user interface
  volatile bool button1;
  volatile bool button2;
  volatile bool button3;
  volatile bool button4;
  volatile bool button5;
  volatile bool button6;
  volatile bool button7;
  volatile bool button8;

  // mode pushbuttons on user interface
  volatile bool buttonmode;
  volatile bool buttonfree;
};

struct ui_out_struct {
  // indication led on user interface
  bool ready_led;  // green led
  bool error_led;  // yellow led

  // 2-line text display
  std::string line1;  // upper text line
  std::string line2;  // lower text line
};

// interface to robot hardware

struct robot_in_struct {
  // limit switches at axes
  // true indicated limit switch pressed
  volatile bool limit_right;
  volatile bool limit_up;
  volatile bool limit_back;
  volatile bool limit_open;

  // delta counters at axes
  // counters indicate the number of ticks since last cycle
  volatile int delta_right_left;
  volatile int delta_up_down;
  volatile int delta_back_fwd;
  volatile int delta_open_close;
};

struct robot_out_struct {
  // motor power commands
  bool motor_right;
  bool motor_up;
  bool motor_back;
  bool motor_open;

  // motor direction commands
  // true sets turn direction as indicated; false sets opposite direction
  bool motor_dir_right;
  bool motor_dir_up;
  bool motor_dir_back;
  bool motor_dir_open;
};


extern bool is_running;
extern int counter;

extern ui_in_struct ui_in;
extern ui_out_struct ui_out;
extern robot_in_struct robot_in;
extern robot_out_struct robot_out;

// control function that computes outputs as a function of inputs
void control();
