#pragma once
#include "RobotControl.h"

// THESE VALUES ARE PURELY FICTIONAL!
// DO NOT USE THESE IN THE REAL CONTROL SOFTWARE
const std::int32_t limit_right = 4096;
const std::int32_t limit_up = 2048;
const std::int32_t limit_back = 128;
const std::int32_t limit_open = 64;

class axis_class {
 public:
  explicit axis_class(std::int32_t limit) : limit{limit} {}
  void perform_step(bool motor_powered,
                    bool dir,
                    volatile bool& limit_switch,
                    volatile int& counter_inc);

 private:
  std::int32_t pos = 0;
  const std::int32_t limit;
  const std::int32_t step_size = 4;
};

class robot_class {
 public:
  void perform_step(const robot_out_struct& robotOut, robot_in_struct& robotIn);

 private:
  axis_class left_right{limit_right};
  axis_class up_down{limit_up};
  axis_class fwd_back{limit_back};
  axis_class open_close{limit_open};
};

static robot_class robot;

void simulate_robot();
