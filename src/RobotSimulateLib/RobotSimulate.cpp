#include "RobotSimulate.h"

void axis_class::perform_step(const bool motor_powered, const bool dir, volatile bool& limit_switch, volatile int& counter_inc)
{
    counter_inc = motor_powered ? step_size : 0;
    pos = dir ? pos + counter_inc : pos - counter_inc;
    limit_switch = pos >= limit;
}

void robot_class::perform_step(const robot_out_struct& robotOut, robot_in_struct& robotIn)
{
    left_right.perform_step(robotOut.motor_right, robotOut.motor_dir_right, robotIn.limit_right, robotIn.delta_right_left);
    up_down.perform_step(robotOut.motor_up, robotOut.motor_dir_up, robotIn.limit_up, robotIn.delta_up_down);
    fwd_back.perform_step(robotOut.motor_back, robotOut.motor_dir_back, robotIn.limit_back, robotIn.delta_back_fwd);
    open_close.perform_step(robotOut.motor_open, robotOut.motor_dir_open, robotIn.limit_open, robotIn.delta_open_close);
}

void simulate_robot()
{
    robot.perform_step(robot_out, robot_in);
}