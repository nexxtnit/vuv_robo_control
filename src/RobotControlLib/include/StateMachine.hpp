#pragma once

class RobotStateMachine{
public:
    typedef enum machineState{  // enum type for the states of the statemachine
        start_up = 0,
        manual_mode = 1,
        automatic_mode = 2,
        shutdown_mode = 3,
        error_mode = 4
    }machineState;

    // basic singleton stuff
    static RobotStateMachine& instance()
    {
       static RobotStateMachine _instance;
       return _instance;
    }
    ~RobotStateMachine() {}
    // public funktions of this class
    void stateMachine(); // switch-case structure of the actual statemachine
    int getState();
    void setState(machineState newState);

private:

    machineState robotState = start_up;
    bool start_up_sequence_ran = false;
    bool shut_down_sequence_ran = false;
    bool buttonMode_Old = false;
    bool buttonFree_Old = false;

    void start_up_sequence();
    void shut_down_sequence();

    // basic singleton stuff
    RobotStateMachine() {}
    RobotStateMachine( const RobotStateMachine& );
    RobotStateMachine & operator = (const RobotStateMachine &);
 };
