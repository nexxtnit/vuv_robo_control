#pragma once

//------------------------------------------------------------------------------
//                              Class RobotMovement
//------------------------------------------------------------------------------

class RobotMovement {
 public:
  static RobotMovement& instance() {
    static RobotMovement _instance;
    return _instance;
  }
  ~RobotMovement(){};

  void executeMovement();
  void stopAllMovement();
  bool startUpRoutine();
  void manualMode();
  void automaticMode();

  struct {
    bool xaxis_clockwise;         // move towards limit switch
    bool xaxis_counterclockwise;  // move away from limit switch
    bool yaxis_pull;
    bool yaxis_push;
    bool zaxis_down;
    bool zaxis_up;
    bool gripper_close;
    bool gripper_open;
  };

 private:
  RobotMovement(){};
  RobotMovement(const RobotMovement&);
  RobotMovement& operator=(const RobotMovement&);
};
