/*
limit_right = 4096;
limit_up = 2048;
limit_back = 128;
limit_open = 64;
*/
#pragma once

#include <array>
#include <iostream>
#include <string>
#include <thread>

#include "Movement.hpp"

class MonitorRobot {
 public:
  // usage: MonitorRobot& monitor = MonitorRobot::instance();
  // or: MonitorRobot::instance().isReady();
  static MonitorRobot& instance() {
    static MonitorRobot _instance;
    return _instance;
  }
  ~MonitorRobot() {}

  bool getIsReady();  // return ready state
  void displayDebugConsole(
      std::string text);               // display text on console with std::cout
  void displayMode(std::string text);  // display text on first line of screen
  void displayErr(std::string text);   // display text on seconde line of screen
  void monitorSystem();                // regular call for monitor system
  void setIsReady(bool);  // set ready state

  bool checkAxisButtons();
  bool checkModeButtons();
  bool checkLimitSwitches();
  bool checkState();
  bool checkDistanceLimits();

 private:
  const std::array<int, 4> distanceLimits{0, 0, 0, 0};  // x=0,y=0,z=0,gripper=0
  std::array<int, 4> distanceAxis;                      // x,y,z,gripper
  bool isReady = false;
  int errorCode = 0;
  std::string messageP1 = "";
  std::string messageP2 = "";
  std::thread thread_;

  void measureDistance();

  void checkErrorState();
  void displayOnLine(std::string text, bool line);
  void setLEDs();         // set LEDs
  void setDisplayText();  // refresh text on display
  RobotMovement& robotMovement =
      RobotMovement::instance();  // include RobotMovement()
  MonitorRobot() {
    ;
  };  // verhindert, dass ein Objekt von außerhalb von N
      // erzeugt wird.
  // protected, wenn man von der Klasse noch erben möchte
  MonitorRobot(
      const MonitorRobot&); /* verhindert, dass eine weitere Instanz via
                               Kopier-Konstruktor erstellt werden kann */
  MonitorRobot& operator=(
      const MonitorRobot&);  // Verhindert weitere Instanz durch Kopie
};
