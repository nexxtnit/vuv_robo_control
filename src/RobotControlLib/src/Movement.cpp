#include "Movement.hpp"

#include "RobotControl.h"
#include "RobotMonitor.hpp"

//------------------------------------------------------------------------------
//                              Stop Movement
//------------------------------------------------------------------------------

void RobotMovement::stopAllMovement() {
  // stop x-axis
  robot_out.motor_right = false;
  // stop y-axis
  robot_out.motor_back = false;
  // stop z-axis
  robot_out.motor_up = false;
  // stop gripper
  robot_out.motor_open = false;
}

//------------------------------------------------------------------------------
//                              Execute Movement
//------------------------------------------------------------------------------

void RobotMovement::executeMovement() {
  MonitorRobot& monitor = MonitorRobot::instance();

  if (monitor.getIsReady()) {
    //--------------------------------------------------------------------------
    // robot x-axis movement (motor right)
    //--------------------------------------------------------------------------

    // x-axis clockwise movement (move right)
    if (xaxis_clockwise && !robot_in.limit_right) {
      robot_out.motor_right = true;
      robot_out.motor_dir_right = true;
    }

    // x-axis counterclockwise movement (move left)
    if (xaxis_counterclockwise) {
      robot_out.motor_right = true;
      robot_out.motor_dir_right = false;
    }

    // x-axis motor off
    if (!xaxis_clockwise && !xaxis_counterclockwise) {
      robot_out.motor_right = false;
    }

    //--------------------------------------------------------------------------
    // robot y-axis movement (motor back)
    //--------------------------------------------------------------------------

    // y-axis pull movement
    if (yaxis_pull && !robot_in.limit_back) {
      robot_out.motor_back = true;
      robot_out.motor_dir_back = true;
    }

    // y-axis push movement
    if (yaxis_push) {
      robot_out.motor_back = true;
      robot_out.motor_dir_back = false;
    }

    // y-axis motor off
    if (!yaxis_pull && !yaxis_push) {
      robot_out.motor_back = false;
    }

    //--------------------------------------------------------------------------
    // robot z-axis movement (motor up)
    //--------------------------------------------------------------------------

    // z-axis up movement
    if (zaxis_up && !robot_in.limit_up) {
      robot_out.motor_up = true;
      robot_out.motor_dir_up = true;
    }

    // z-axis down movement
    if (zaxis_down) {
      robot_out.motor_up = true;
      robot_out.motor_dir_up = false;
    }

    // z-axis motor off
    if (!zaxis_down && !zaxis_up) {
      robot_out.motor_up = false;
    }

    //--------------------------------------------------------------------------
    // robot gripper movement (motor open)
    //--------------------------------------------------------------------------

    // gripper close movement
    if (gripper_close) {
      robot_out.motor_open = true;
      robot_out.motor_dir_open = false;
    }

    // gripper open movement
    if (gripper_open && !robot_in.limit_open) {
      robot_out.motor_open = true;
      robot_out.motor_dir_open = true;
    }

    // gripper motor off
    if (!gripper_close && !gripper_open) {
      robot_out.motor_open = false;
    }

  } else {
    stopAllMovement();
  }
}

//------------------------------------------------------------------------------
//                              Startup Routine
//------------------------------------------------------------------------------

bool RobotMovement::startUpRoutine() {
  xaxis_clockwise = true;
  yaxis_pull = true;
  zaxis_up = true;
  gripper_open = true;
  executeMovement();
  if (robot_in.limit_right && robot_in.limit_back && robot_in.limit_up &&
      robot_in.limit_open)
    return true;
  else
    return false;
}

//------------------------------------------------------------------------------
//                              Manual Mode
//------------------------------------------------------------------------------

void RobotMovement::manualMode() {
  //--------------------------------------------------------------------------
  // x-axis button press assignment
  //--------------------------------------------------------------------------

  // x-axis clockwise movement (move right)
    if (!ui_in.button1 && ui_in.button2) {
      xaxis_clockwise = true;
      xaxis_counterclockwise = false;
    }


    // x-axis counterclockwise movement (move left)
  if (ui_in.button1 && !ui_in.button2) {
    xaxis_clockwise = false;
    xaxis_counterclockwise = true;
  }

  // x-axis motor off
  if (!ui_in.button1 && !ui_in.button2) {
    xaxis_clockwise = false;
    xaxis_counterclockwise = false;
  }

  //--------------------------------------------------------------------------
  // y-axis button press assignment
  //--------------------------------------------------------------------------

  // y-axis push movement
  if (ui_in.button5 && !ui_in.button6) {
    yaxis_push = true;
    yaxis_pull = false;
  }

  // y-axis pull movement
  if (!ui_in.button5 && ui_in.button6) {
    yaxis_push = false;
    yaxis_pull = true;
  }

  // y-axis motor off
  if (!ui_in.button5 && !ui_in.button6) {
    yaxis_push = false;
    yaxis_pull = false;
  }

  //--------------------------------------------------------------------------
  // z-axis button press assignment
  //--------------------------------------------------------------------------

  // z-axis down movement
  if (!ui_in.button3 && ui_in.button4) {
    zaxis_up = false;
    zaxis_down = true;
  }

  // z-axis up movement
  if (ui_in.button3 && !ui_in.button4) {
    zaxis_up = true;
    zaxis_down = false;
  }

  // z-axis motor off
  if (!ui_in.button3 && !ui_in.button4) {
    zaxis_up = false;
    zaxis_down = false;
  }

  //--------------------------------------------------------------------------
  // gripper button press assignment
  //--------------------------------------------------------------------------

  // gripper close movement
  if (ui_in.button7 && !ui_in.button8) {
    gripper_close = true;
    gripper_open = false;
  }

  // gripper open movement
  if (!ui_in.button7 && ui_in.button8) {
    gripper_close = false;
    gripper_open = true;
  }

  // gripper motor off
  if (!ui_in.button7 && !ui_in.button8) {
    gripper_close = false;
    gripper_open = false;
  }

  executeMovement();
}

//------------------------------------------------------------------------------
//                              Automatic Mode
//------------------------------------------------------------------------------

void RobotMovement::automaticMode() {}
