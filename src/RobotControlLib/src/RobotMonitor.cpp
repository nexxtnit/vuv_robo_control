#include "RobotMonitor.hpp"

#include "RobotControl.h"
#include "StateMachine.hpp"

bool MonitorRobot::getIsReady() {
  return isReady;
}

void MonitorRobot::setIsReady(bool state) {
  isReady = state;
}

void MonitorRobot::setLEDs() {
  if (isReady)
    ui_out.ready_led = true;
  if (!isReady)
    ui_out.ready_led = false;
  if (errorCode == 0)
    ui_out.error_led = false;
  if (errorCode != 0)
    ui_out.error_led = true;
}

void MonitorRobot::displayDebugConsole(std::string text) {
  std::cout << text << std::endl;
}

void MonitorRobot::displayMode(std::string text) {
  displayOnLine(text, 0);
}
void MonitorRobot::displayErr(std::string text) {
  displayOnLine(text, 1);
}

void MonitorRobot::displayOnLine(std::string text, bool line) {
  // Maybe check for length of text so it fits in display
  if (!line)
    messageP1 = text;
  if (line)
    messageP2 = text;
}
void MonitorRobot::measureDistance() {
  // track x-axis /right-left
  if (robot_in.limit_right) {
    distanceAxis[0] = 4096;
  } else {
    if (robotMovement.xaxis_clockwise)
      distanceAxis[0] += robot_in.delta_right_left;
    if (robotMovement.xaxis_counterclockwise)
      distanceAxis[0] -= robot_in.delta_right_left;
  }
  // track y-axis/forward-backwards
  if (robot_in.limit_back) {
    distanceAxis[1] = 128;
  } else {
    if (robotMovement.yaxis_pull)
      distanceAxis[1] += robot_in.delta_back_fwd;
    if (robotMovement.yaxis_push)
      distanceAxis[1] -= robot_in.delta_back_fwd;
  }

  // track z-axis /up-down
  if (robot_in.limit_up) {
    distanceAxis[2] = 2048;
  } else {
    if (robotMovement.zaxis_up)
      distanceAxis[2] += robot_in.delta_up_down;
    if (robotMovement.zaxis_down)
      distanceAxis[2] -= robot_in.delta_up_down;
  }
  // track gripper open-close
  if (robot_in.limit_open) {
    distanceAxis[3] = 64;
  } else {
    if (robotMovement.gripper_open) {
      distanceAxis[3] += robot_in.delta_open_close;
    }
    if (robotMovement.gripper_close) {
      distanceAxis[3] -= robot_in.delta_open_close;
    }
  }

}
void MonitorRobot::setDisplayText() {
  ui_out.line1 = messageP1;
  ui_out.line2 = messageP2;
}
bool MonitorRobot::checkModeButtons() {
  if (ui_in.buttonfree && ui_in.buttonmode) {
    errorCode = 1;
    setIsReady(false);
    return false;
  } else {
    return true;
  }
}
bool MonitorRobot::checkAxisButtons() {
  if (ui_in.button1 && ui_in.button2) {
    setIsReady(false);
    errorCode = 2;
  } else if (ui_in.button3 && ui_in.button4) {
    setIsReady(false);
    errorCode = 2;
  } else if (ui_in.button5 && ui_in.button6) {
    setIsReady(false);
    errorCode = 2;
  } else if (ui_in.button7 && ui_in.button8) {
    setIsReady(false);
    errorCode = 2;
  } else {
    return true;
  }
  return false;
}
bool MonitorRobot::checkLimitSwitches() {
  if (robot_in.limit_up && robotMovement.zaxis_up) {
    robotMovement.zaxis_up = false;
    robotMovement.executeMovement();
    errorCode = 31;
  } else if (robot_in.limit_right && robotMovement.xaxis_clockwise) {
    robotMovement.xaxis_clockwise = false;
    robotMovement.executeMovement();
    errorCode = 32;
  } else if (robot_in.limit_back && robotMovement.yaxis_pull) {
    robotMovement.yaxis_pull = false;
    robotMovement.executeMovement();
    errorCode = 33;
  } else if (robot_in.limit_open && robotMovement.gripper_open) {
    robotMovement.gripper_open = false;
    robotMovement.executeMovement();
    errorCode = 34;
  } else
    return true;
  return false;
}
bool MonitorRobot::checkDistanceLimits() {
  if ((distanceAxis[0] <= distanceLimits[0]) && ui_in.button1) {
    setIsReady(false);
    errorCode = 41;
  } else if ((distanceAxis[1] <= distanceLimits[1]) && ui_in.button5) {
    setIsReady(false);
    errorCode = 42;
  } else if ((distanceAxis[2] <= distanceLimits[2]) && ui_in.button4) {
    setIsReady(false);
    errorCode = 43;
  } else if ((distanceAxis[3] <= distanceLimits[3]) && ui_in.button7) {
    setIsReady(false);
    errorCode = 44;
  } else
    return true;
  return false;
}
bool MonitorRobot::checkState() {
  RobotStateMachine& robotStateMachine = RobotStateMachine::instance();
  int robotState = robotStateMachine.getState();
  std::string states[] = {"start_up", "manual_mode", "automatic_mode",
                          "shutdown_mode", "error_mode"};
  displayMode(states[robotState]);

  if (robotState == 4) {
    setIsReady(false);
    errorCode = 5;
    return false;
  } else

    return true;
}

void MonitorRobot::checkErrorState() {
  switch (errorCode) {
    case 0:
      setIsReady(true);
      displayErr("System OK");
      break;
    case 1:
      displayErr("Err1: both mode buttons pressed");
      break;
    case 2:
      displayErr("Err2: both buttons on same axis pressed");
      break;
    case 31:
      displayErr("Err31: limit Switch UP reached");
      break;
    case 32:
      displayErr("Err32: limit Switch RIGHT reached");
      break;
    case 33:
      displayErr("Err33: limit Switch BACK reached");
      break;
    case 34:
      displayErr("Err34: limit Switch OPEN reached");
      break;
    case 41:
      displayErr("Err41: max distance X-axis reached");
      break;
    case 42:
      displayErr("Err41: max distance Y-axis reached");
      break;
    case 43:
      displayErr("Err41: max distance Z-axis reached");
      break;
    case 44:
      displayErr("Err41: max distance gripper reached");
      break;
    case 5:
      displayErr("Err4: Shutdown was incorrect");
      break;
    default:
      break;
  }
}
// monitors system constantly and
void MonitorRobot::monitorSystem() {
  checkErrorState();
  measureDistance();
  setLEDs();
  setDisplayText();
  checkState();
  if (checkModeButtons() && checkAxisButtons() && checkLimitSwitches() &&
      checkState() && checkDistanceLimits())
    errorCode = 0;
}
