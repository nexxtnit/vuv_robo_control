#include "StateMachine.hpp"

#include <filesystem>
#include <iostream>

#include "Movement.hpp"
#include "RobotControl.h"
#include "RobotMonitor.hpp"

RobotMovement& robotMovement = RobotMovement::instance();

int RobotStateMachine::getState() {
  return this->robotState;
}
void RobotStateMachine::setState(machineState newState) {
  this->robotState = newState;
}
void RobotStateMachine::start_up_sequence() {
    bool exists = std::filesystem::exists("log.txt");
    if (!exists) {
        this->start_up_sequence_ran = robotMovement.startUpRoutine();
        if (start_up_sequence_ran){
            FILE* fp;
            fp = fopen("log.txt", "w+");
            fprintf(fp, "This is a log file");
            fclose(fp);
            this->shut_down_sequence_ran = false;
            std::cout << "startup finished" << std::endl;
        }
    }
    else {
        this->robotState = error_mode;
    }
}
void RobotStateMachine::shut_down_sequence() {
    this->shut_down_sequence_ran = robotMovement.startUpRoutine(); // running the startUpRoutine again, for good measure
    if(shut_down_sequence_ran){
        this->start_up_sequence_ran = false;
        std::cout << "shutdown finished" << std::endl;
        remove("log.txt");
    }
}
void RobotStateMachine::stateMachine(){
  switch (this->robotState) {
    case start_up:
      if(!start_up_sequence_ran){
        start_up_sequence();
      }
      if(start_up_sequence_ran){
          this->robotState = manual_mode;
      }
      else{
          break;
      }

    case manual_mode:
      if (buttonMode_Old == false && ui_in.buttonmode) {
        this->robotState = automatic_mode;
        buttonMode_Old = ui_in.buttonmode;
        break;
      } else if (buttonMode_Old == true && ui_in.buttonmode == false) {
        buttonMode_Old = ui_in.buttonmode;
        break;
      }
      if (buttonFree_Old == false && ui_in.buttonfree && ui_in.button3) {
        this->robotState = shutdown_mode;
        buttonFree_Old = ui_in.buttonfree;
        break;
      } else if (buttonFree_Old == true && ui_in.buttonfree == false) {
        buttonFree_Old = ui_in.buttonfree;
        break;
      }
      else{
        robotMovement.manualMode();
        break;
      }

    case automatic_mode:
      if (buttonMode_Old == false && ui_in.buttonmode) {
        this->robotState = manual_mode;
        buttonMode_Old = ui_in.buttonmode;
        break;
      } else if (buttonMode_Old == true && ui_in.buttonmode == false) {
        buttonMode_Old = ui_in.buttonmode;
        break;
      }
      if (buttonFree_Old == false && ui_in.buttonfree && ui_in.button3) {
        this->robotState = shutdown_mode;
        buttonFree_Old = ui_in.buttonfree;
        break;
      } else if (buttonFree_Old == true && ui_in.buttonfree == false) {
        buttonFree_Old = ui_in.buttonfree;
        break;
      }
      else{
          //automatic movement here
          break;
      }

  case shutdown_mode:
      if(!shut_down_sequence_ran){
          shut_down_sequence();
      }
      if (buttonFree_Old == false && ui_in.buttonfree && shut_down_sequence_ran) {
        this->robotState = start_up;
        buttonFree_Old = ui_in.buttonfree;
        break;
      } else if (buttonFree_Old == true && ui_in.buttonfree == false) {
        buttonFree_Old = ui_in.buttonfree;
        break;
      }else{
          break;
      }

    case error_mode:
      if (buttonFree_Old != ui_in.buttonfree) {
        remove("log.txt");
        this->robotState = start_up;
        buttonFree_Old = ui_in.buttonfree;
        break;
      } else if (buttonFree_Old == true && ui_in.buttonfree == false) {
        buttonFree_Old = ui_in.buttonfree;
        break;
      }
      else {
        break;
      }

    default:
      std::cout << "ERROR THIS STATE MAKES NO SENSE\nHOW DID THIS "
                   "HAPPEN\nREEEEEEEEEEEEEEEEEEEE"
                << std::endl;
      break;
  }
}
