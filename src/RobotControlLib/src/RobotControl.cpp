#include "RobotControl.h"

#include "Movement.hpp"
#include "RobotMonitor.hpp"
#include "StateMachine.hpp"

void control() {
  RobotStateMachine& robotStateMachine = RobotStateMachine::instance();
  MonitorRobot& monitor = MonitorRobot::instance();

  monitor.monitorSystem();
  robotStateMachine.stateMachine();
}